import React, {useState} from "react";
import TareaFormulario from "./Formulario";
import Tarea from "./Tarea";

function ListaDeTareas(){
    
    const [tareas, setTareas] = useState([]);
    const agregarTarea = tarea => {
        if(tarea.texto.trim()){
            tarea.texto = tarea.texto.trim();
            const tareasActualizadas = [tarea, ...tareas];
            setTareas(tareasActualizadas);
        }
    }

    const eliminarTarea = id =>{
        const tareasActualizadas = tareas.filter(tarea => tarea.is !== id);
        setTareas(tareasActualizadas);
    }
    
    return(
        <>
            <TareaFormulario  onSubmit={agregarTarea}/>
            <div className="tareas-lista-contenedor">
                {
                    tareas.map((tarea) => 
                        <Tarea 
                            texto={tarea.texto}
                            key={tarea.id}
                            id={tarea.id}
                            completada={tarea.completada}
                            eliminarTarea={eliminarTarea}
                        />
                    )
                }
            </div>
        </>
    );
}

export default ListaDeTareas;