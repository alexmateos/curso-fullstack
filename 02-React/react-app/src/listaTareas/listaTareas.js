import React from "react";
import "./media/styles.css"
import Tarea from "./Tarea";
import TareaFormulario from "./Formulario";
import ListaDeTareas from "./ListaDeTareas";

function ListaTareas(){
    return(
        <div className="tareas-lista-principal">
            <h1>Tareas</h1>
            <ListaDeTareas></ListaDeTareas>
        </div>
    )
}

export default ListaTareas;