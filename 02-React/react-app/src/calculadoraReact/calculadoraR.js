import React from "react";
import PropTypes from "prop-types";
import "./styles.css";

export class Calculadora extends React.Component {
    constructor(props) {
        super(props);
        this.calculadora = {
            valorPantalla: '0',
            primerNum: null,
            segundoNum: null,
            operador: null,
            banderaSegundo: false,
        };
        this.state = { pantalla: this.props.valorPantalla }
        this.meterNumero = this.meterNumero.bind(this);
        this.operar = this.operar.bind(this);
        this.meterDecimal = this.meterDecimal.bind(this);
        this.negativo = this.negativo.bind(this);
        this.borrar = this.borrar.bind(this);
        this.borrarTodo = this.borrarTodo(this);

    }
    actualizar() {
        this.setState({ pantalla: this.calculadora.valorPantalla })
    }

    meterNumero(event) {
        const pantalla = this.calculadora.valorPantalla;
        const banderaSegundo = this.calculadora.banderaSegundo;
        const numero = event.target.value;
        if (banderaSegundo === true) {
            this.calculadora.valorPantalla = numero;
            this.calculadora.banderaSegundo = false;
        } else 
            this.calculadora.valorPantalla = pantalla === '0' ? numero : (pantalla + numero);
        this.actualizar();
    }

    operar(event) {
        const pantalla = this.calculadora.valorPantalla;
        const primerNum = this.calculadora.primerNum;
        const operador = this.calculadora.operador;
        const simbolo = event.target.value

        const valorNum = parseFloat(pantalla);

        if (primerNum === null && !isNaN(pantalla)) {
            this.calculadora.primerNum = valorNum;
        } else if (operador) {
            const resultado = this.calcular(primerNum, valorNum, operador);
            this.calculadora.valorPantalla = String(resultado);
            this.calculadora.primerNum = resultado;
        }

        this.calculadora.banderaSegundo = true;
        this.calculadora.operador = simbolo;

        this.actualizar();
    }

    calcular(primerNum, segNum, operador) {
        switch (operador) {
            case "+":
                return primerNum + segNum;
            case "-":
                return primerNum - segNum;
            case "x":
                return primerNum * segNum;
            case "%":
                return primerNum / segNum;
            case "=":
                return segNum;
        }
    }

    borrar() {
        this.calculadora.valorPantalla = "0";
        this.actualizar();
    }

    borrarTodo() {
        this.calculadora.valorPantalla = "0";
        this.calculadora.primerNum = null;
        this.calculadora.segundoNum = null;
        this.calculadora.banderaSegundo = false;
        this.calculadora.operador = null;
        this.actualizar();
    }

    negativo() {
        if (!this.calculadora.valorPantalla.includes("-")) {
            this.calculadora.valorPantalla = "-" + this.calculadora.valorPantalla;
        } else this.calculadora.valorPantalla = this.calculadora.valorPantalla.substring(1);

        this.actualizar();
    }

    meterDecimal(event) {
        const decimal = event.target.value;
        if (!this.calculadora.valorPantalla.includes(decimal)) {
            this.calculadora.valorPantalla += decimal;
        }
        this.actualizar();
    }

    render() {
        return (
            <div>
                <h1>Calculadora</h1>

                <section className="calculadora">
                    <div className="pantalla">{this.state.pantalla}</div>

                    <input type="button" value="+" className="operador" onClick={this.operar} />
                    <input type="button" value="-" className="operador" onClick={this.operar} />
                    <input type="button" value="x" className="operador" onClick={this.operar} />
                    <input type="button" value="%" className="operador" onClick={this.operar} />

                    <input type="button" value="7" className="numero" onClick={this.meterNumero} />
                    <input type="button" value="8" className="numero" onClick={this.meterNumero} />
                    <input type="button" value="9" className="numero" onClick={this.meterNumero} />
                    <input type="button" value="CE" className="clearAll" onClick={this.borrarTodo} />

                    <input type="button" value="4" className="numero" onClick={this.meterNumero} />
                    <input type="button" value="5" className="numero" onClick={this.meterNumero} />
                    <input type="button" value="6" className="numero" onClick={this.meterNumero} />
                    <input type="button" value="C" className="clear" onClick={this.borrar} />

                    <input type="button" value="1" className="numero" onClick={this.meterNumero} />
                    <input type="button" value="2" className="numero" onClick={this.meterNumero} />
                    <input type="button" value="3" className="numero" onClick={this.meterNumero} />
                    <input type="button" value="=" className="igual" onClick={this.operar} />

                    <input type="button" value="+/-" className="negativo" onClick={this.negativo} />
                    <input type="button" value="0" className="numero" onClick={this.meterNumero} />
                    <input type="button" value="," className="decimal" onClick={this.meterDecimal} />

                </section>

            </div>
        );
    }
}

