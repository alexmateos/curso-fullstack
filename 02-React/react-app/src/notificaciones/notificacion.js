import React from "react";

function Notificacion({mensaje, id, eliminarNotificacion}){
    return(
        <div className="notificacion">
            <div className="notificacion-texto">{mensaje}</div>
            <button className="icono"
            onClick={() => eliminarNotificacion(id)}>X</button>
        </div>
    );
}

export default Notificacion;