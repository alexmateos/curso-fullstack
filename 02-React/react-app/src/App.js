import './App.css';
import FotoMuro from './galeriaFotos/galeria';
import { Calculadora } from './calculadoraReact/calculadoraR';
import {BrowserRouter, Link, Route, Routes} from "react-router-dom"
import { ErrorBoundary, PageNotFound } from './comunes/comunes';
import ContadorClicks from './contadorClicks/contadorClick';
import PersonasRoute from './personas/personas';


export default function App() {
  return(
    <BrowserRouter>
      <nav>
      &nbsp;|&nbsp;
        <Link to="/">Inicio</Link>&nbsp;|&nbsp;
        <Link to="/personas">Personas</Link>&nbsp;|&nbsp;
        <Link to="/calculadora">Calculadora</Link>&nbsp;|&nbsp;
        <Link to="/muro">Fotos</Link>&nbsp;|&nbsp;
        <Link to="/formulario">Formulario</Link>&nbsp;|&nbsp;
      </nav>
      <div className='container-fluid'>
        <ErrorBoundary>
          <Routes>
            <Route path='/' element={<ContadorClicks/>} />
            <Route path="/calculadora" element={<Calculadora/>} />
            <Route path="/muro" element={<FotoMuro/>} />
            <Route path='/personas' element={<PersonasRoute/>}>
              
            </Route>
            <Route path='*' element={<PageNotFound/>} />
          </Routes>
        </ErrorBoundary>
      </div>
    </BrowserRouter>
  );  
}
