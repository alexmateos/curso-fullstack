import { createSlice } from '@reduxjs/toolkit';

export const notificacionSlice = createSlice({
    name: "notificacion",
    initialState: [],
    reducers:{
        nueva: (state, action) => {state.push(action.payload)},
        eliminar: (state, action) => {state.splice(action.payload, 1)},
        eliminarTodo: (state) => {state = []},
    }
});

export const {nueva, eliminar, eliminarTodo} = notificacionSlice.actions;

export default notificacionSlice.reducer;
