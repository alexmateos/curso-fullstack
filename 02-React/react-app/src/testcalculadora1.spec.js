// Generated by Selenium IDE
const { Builder, By, Key, until } = require('selenium-webdriver')
const assert = require('assert')

describe('test calculadora 1', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('test calculadora 1', async function() {
    await driver.get("http://localhost:3001/calculadora")
    await driver.manage().window().setRect({ width: 1440, height: 779 })
    await driver.findElement(By.css(".numero:nth-child(6)")).click()
    assert(await driver.findElement(By.css(".calculadora")).getText() == "7")
    await driver.findElement(By.css(".operador:nth-child(3)")).click()
    await driver.findElement(By.css(".numero:nth-child(12)")).click()
    assert(await driver.findElement(By.css(".calculadora")).getText() == "6")
    await driver.findElement(By.css(".igual")).click()
    assert(await driver.findElement(By.css(".calculadora")).getText() == "1")
    await driver.findElement(By.css(".clear")).click()
    assert(await driver.findElement(By.css(".calculadora")).getText() == "0")
    await driver.findElement(By.css(".numero:nth-child(11)")).click()
    assert(await driver.findElement(By.css(".calculadora")).getText() == "5")
    await driver.findElement(By.css(".numero:nth-child(7)")).click()
    assert(await driver.findElement(By.css(".calculadora")).getText() == "58")
    await driver.findElement(By.css(".operador:nth-child(2)")).click()
    await driver.findElement(By.css(".numero:nth-child(14)")).click()
    assert(await driver.findElement(By.css(".calculadora")).getText() == "1")
    await driver.findElement(By.css(".igual")).click()
    assert(await driver.findElement(By.css(".calculadora")).getText() == "59")
  })
})
