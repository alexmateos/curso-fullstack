import React, { Component } from "react";
import Boton from "./boton";
import Contador from "./contador";
import './media/styles.css'
import { useState } from "react";

function ContadorClicks() {
    
    const [numClicks, setNumclicks] = useState(0);

     const handleClick = () => {
        setNumclicks(numClicks + 1);
    }

     const reiniciarContador = () => {
        setNumclicks(0);
    }

        return(
            <div className="contenedor">
                <Contador numClicks={numClicks}/>
                <Boton texto="Click"
                botonClick={true}
                handleClick={handleClick}/>
                <Boton texto="Reiniciar"
                botonClick={false}
                handleClick={reiniciarContador}/>
            </div>
        );
}

export default ContadorClicks;