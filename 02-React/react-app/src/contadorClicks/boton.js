import React from "react";

function Boton({texto, botonClick, handleClick}){
    return(
        <button className={ botonClick ? "boton-click" : "boton-reiniciar" }
        onClick={handleClick}>
            {texto}
        </button>
    )
}

export default Boton;