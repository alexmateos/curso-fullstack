var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.route('/login')
    .get(function (req, res) {
        res.render('login', { user: "", password: "P@$$w0rd" } )
    })
    .post(async function (req, res) {
        if(req.body.user === "admin" && req.body.password === "P@$$w0rd") {
            if(req.query.continue)
            res.redirect(req.query.continue)
            else
            res.redirect('/')
        } else 
          res.render('login', {user: req.body.user, password: req.body.password} )
    })
module.exports = router;
