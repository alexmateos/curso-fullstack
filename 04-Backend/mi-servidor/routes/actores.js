const express = require('express');
const { Sequelize, DataTypes, Op, QueryTypes } = require('sequelize');
const { formatError, formatLocation } = require('../lib/data')
const security = require("../lib/security");
const initModels = require("../models/init-models");
const sequelize = new Sequelize('mysql://root:root@localhost:3306/sakila')
const dbContext = initModels(sequelize);

var router = express.Router();
// router.use(security.onlyInRole('Empleados,Administrador'));

router.route('/')
  .get(async function(request, response){
    const pagina = +request.query.pagina || 0;
        const limit = +request.query.rows || 10;
        let options = { offsset: pagina * limit, limit }
        if (request.query.pagina === 'all') {
            options = {}
        }
        if (request.query.proyection) {
            options.attributes = request.query.proyection.split(',')
        } else {
            options.attributes = ["actor_id", "first_name", "last_name"]
        }
        if (request.query.order) {
            options.order = request.query.order.split(',')
        }
        if (request.query.find) {
            options.where = { first_name: { [Op.startsWith]: request.query.find } }
        }
        try {
            let resultado = await dbContext.actor.findAll(options)
            response.json(resultado)
        } catch (error) {
            response.status(400).json(formatError(error))
        }

  })
  .post(async function (req, res) {
    let row = await dbContext.actor.build({ first_name: req.body.first_name, last_name: req.body.last_name })
    try {
        await row.validate()
        await row.save()
        res.append('location', formatLocation(req, row.actor_id))
        res.sendStatus(201)
    } catch (error) {
        res.status(400).send(formatError(error))
    }
  })

router.route('/:id')
  .get(async function (req, res) {
      try {
          let row = await dbContext.actor.findByPk(req.params.id)
          if (row)
              res.json(row)
          else
              res.sendStatus(404)
      } catch {
          res.sendStatus(404)
      }
  })
  .put(async function (req, res) {
    if (req.body.actor_id && req.body.actor_id != req.params.id) {
        res.status(400).json({ message: 'Invalid identifier' })
        return
    }
    let row = await dbContext.actor.findByPk(req.params.id)
    if (!row) {
        res.sendStatus(404)
        return
    }
    row.set({ first_name: req.body.first_name, last_name: req.body.last_name })
    try {
        await row.save()
        res.sendStatus(204)
    } catch (error) {
        res.status(400).send(formatError(error))
    }
  })
  .delete(async function (req, res) {
      let row = await dbContext.actor.findByPk(req.params.id)
      if (!row) {
          res.sendStatus(404)
          return
      }
      try {
          await row.destroy()
          res.sendStatus(204)
      } catch (error) {
          res.status(409).json(formatError(error, 409))
      }
  })

router.get('/:id/peliculas', async function (req, res) {
  try {
      let row = await dbContext.actor.findByPk(req.params.id, { include: 'peliculas' })
      if (row)
          res.json(row.peliculas.map(item => ({ id: item.film_id, name: item.title })))
      else
          res.sendStatus(404)
  } catch {
      res.sendStatus(404)
  }
})

module.exports = router;
