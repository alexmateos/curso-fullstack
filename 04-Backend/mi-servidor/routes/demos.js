var express = require('express');
var router = express.Router();

router.all('/', function (req, res, next) {
    res.send('Es una demo').end()
});
router.all('/kkk', function (req, res, next) {
    res.send('Esto es una kkk').end()
});
router.all('/kk*/:ip-:port', function (req, res, next) {
    res.send('Esto es una ' + req.params.ip + ':' +  req.params.port).end()
});
router.all('/kk**', function (req, res, next) {
    res.send('Esto es una kk').end()
});
    
module.exports = router;