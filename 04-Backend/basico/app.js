const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017';
const dbName = 'curso';

const client = new MongoClient(url);

client.connect(function (err) {
    if(err) throw err;
    const db = client.db(dbName);
    const collection = db.collection('personas');
    collection.find({}).toArray(function (err, docs) {
        if(err) throw err;
        console.log(docs);
        client.close();
    });
});
