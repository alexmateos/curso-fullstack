const { Sequelize, DataTypes, Op, QueryTypes } = require('sequelize');
const initModels = require("./models/init-models");
const sequelize = new Sequelize('mysql://root:root@localhost:3306/sakila');
const dbContext = initModels(sequelize);

console.log("hola");

async function encuentraUno(){
    let row = await dbContext.actor.findOne({where: {actor_id: {[Op.lt]:10}}})
    console.log(row.toJSON())
}

async function encuentraMuchos(){
    let rows = await dbContext.actor.findAll({ where: {actor_id: { [Op.gt]: 194 }} });
    rows.forEach(row =>{
        console.log(row.toJSON())
    });
}

async function encuentraPaginas(page=0, limit=20){
    let rows = await dbContext.actor.findAll({offset: page*limit, limit, order: ['first_name', 'last_name']});
    console.log(rows.map(row =>({id: row.actor_id, name: row.first_name + ' ' + row.last_name})))
}

async function encuentraAsociaciones() {
    let row = await dbContext.actor.findByPk(1, { include: 'films' })
    let rows = row.films
    console.log({ id: row.actor_id, name: row.first_name + ' ' + row.last_name, films: rows.map(item => ({ id: item.film_id, name: item.title })) })
}

async function insert() {
    let row = await dbContext.actor.build({ first_name: 'John', last_name: 'Doe' })
    try {
        await row.validate()
    } catch (error) {
        console.log('400 Datos inválidos')
        console.log(error.errors) 
        console.log({
            type: "https://tools.ietf.org/html/rfc7231#section-6.5.1",
            status: 400,
            title: 'One or more validation errors occurred.',
            errors: Object.assign({}, ...error.errors.map(item => ({ [item.path]: item.message })))
        })
        return;
    }
    console.log(row)
    await encuentraMuchos()
}

async function update() {
    let row = await dbContext.actor.findOne({ order: [['actor_id', 'DESC']] })
    console.log(row)
    row.first_name = row.first_name.toUpperCase()
    console.log('Guardo ...')
    await row.save()
    console.log(row)
    await encuentraMuchos()
}

async function remove() {
    let row = await dbContext.actor.findOne({ order: [['actor_id', 'DESC']] })
    console.log(row)
    await row.destroy()
    console.log(row)
    await encuentraMuchos()
}


//encuentraUno().then(() =>{ console.log(`Ya está ${new Date().toLocaleDateString('es')}`);process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))
//encuentraMuchos().then(() =>{ console.log(`Ya está ${new Date().toLocaleDateString('es')}`);process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))
//encuentraPaginas(2, 10).then(() =>{ console.log(`Ya está ${new Date().toLocaleDateString('es')}`);process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))