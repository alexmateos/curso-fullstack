const calculadora = {
    valorPantalla: "0",
    primerNum: null,
    segundoNum: null,
    operador: null,
    banderaSegundo: false,
};

function actualizar(){
    const pantalla = document.querySelector(".pantalla");
    pantalla.textContent = calculadora.valorPantalla;
}

function meterNumero(numero){
    const pantalla = calculadora.valorPantalla;
    const banderaSegundo = calculadora.banderaSegundo;
    
    if(banderaSegundo === true){
        calculadora.valorPantalla = numero;
        calculadora.banderaSegundo = false;
        console.log(calculadora.pantalla);
    }else calculadora.valorPantalla = pantalla === '0' ? numero : pantalla+numero;
    console.log(calculadora);
}

function meterDecimal(decimal){
    if(!calculadora.valorPantalla.includes(decimal)){
        calculadora.valorPantalla += decimal;
    }
}

function operar(simbolo){
    const pantalla = calculadora.valorPantalla;
    const primerNum  = calculadora.primerNum;
    const operador = calculadora.operador;

    const valorNum = parseFloat(pantalla);

    if(primerNum === null && !isNaN(pantalla)){
        calculadora.primerNum = valorNum;
    } else if (operador){
      const resultado  = calcular(primerNum,valorNum,operador);
      calculadora.valorPantalla  = String(resultado);
      calculadora.primerNum  = resultado;
    }

    calculadora.banderaSegundo = true;
    calculadora.operador = simbolo;
    console.log(calculadora);
}

function borrar(){
  calculadora.valorPantalla = "0";
}

function borrarTodo(){
  calculadora.valorPantalla = "0";
  calculadora.primerNum = null;
  calculadora.segundoNum = null;
  calculadora.banderaSegundo = false;
  calculadora.operador  = null;
}

function negativo(){
  if(!calculadora.valorPantalla.includes("-")){
    calculadora.valorPantalla = "-" + calculadora.valorPantalla;
  } else calculadora.valorPantalla = calculadora.valorPantalla.substring(1);
  
  //calculadora.valorPantalla = -(calculadora.valorPantalla).toString();
}

function calcular(primerNum, segNum,operador){
  switch (operador){
    case "+":
      return primerNum+segNum;
    case "-":
      return primerNum-segNum;
    case "x":
      return primerNum*segNum;
    case "%":
      return primerNum/segNum;
    case "=":
      return segNum;
  }
}

actualizar();

const numeros = document.querySelector(".calculadora");
numeros.addEventListener("click", (evento)=>{
    const target = evento.target; 

    if (!target.matches('input')) {
        return;
      }
    
      if (target.classList.contains('operador')) {
        operar(target.value);
        actualizar();
        return;
      }
    
      if (target.classList.contains('igual')) {
        operar(target.value);
        actualizar();
        return;
      }
    
      if (target.classList.contains('clear')) {
        borrar();
        actualizar();
        return;
      }

      if (target.classList.contains('clearAll')) {
        borrarTodo();
        actualizar();
        return;
      }

      if (target.classList.contains('decimal')) {
        meterDecimal(target.value);
        actualizar();
        return;
      }

      if (target.classList.contains('negativo')) {
        negativo();
        actualizar();
        return;
      }

      meterNumero(target.value);
      actualizar();
});



