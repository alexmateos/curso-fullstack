/* EJERCICIOS JAVASCRIPT */

//1.-FUNCION QUE DEVUELVE UN NUMERO ALEATORIO

function numAleatorio(min,max){
    if(min>max){
        return "El minimo no puede ser mayor que el máximo"
    }else{
        return Math.round(min + Math.random()*(max-min));
    }
}

/*

let min = window.prompt("Introduce el número mínimo");
let max = window.prompt("Introduce el número máximo");

alert("Numero aleatorio entre " + min + " y " + max + "----> " + numAleatorio(min,max))

*/

//2.- ADIVINA EL NUMERO

function adivinarNumero(){
    let cont = 1;
    let exito = false;
    const MAX_CONT = 10;
    const MIN_ADIVINAR = 1;
    const MAX_ADIVINAR = 100;
    let adivinar = numAleatorio(MIN_ADIVINAR, MAX_ADIVINAR);

    window.confirm("Vamos a jugar a ADIVINA EL NÚMERO. Voy a pensar un número entre el " + MIN_ADIVINAR + " y el " + MAX_ADIVINAR + ". Tienes " + MAX_CONT + " intentos, empezamos?");

    do{
        
        let intento = parseInt(window.prompt("Escribe un número entre el 1 y el 100. Número de intentos restantes: " + cont), "0");
        if(intento != adivinar && intento < adivinar){
            exito = false;
            alert("Has fallado, sigue intentándolo... PISTA: El número que buscas es MAYOR que " + intento);
            cont++;
        }else if(intento != adivinar && intento > adivinar){
            exito = false;
            alert("Has fallado, sigue intentándolo... PISTA: El número que buscas es MENOR que " + intento);
            cont++
        }else exito = true;
       console.log(intento);
       console.log(typeof(intento));

    }while (cont != MAX_CONT && exito == false)

    if(exito == true){
        alert("ENHORAUENA! Has ganado, el número que buscabas es el " + adivinar)
    }else alert("QUE LÁSTIMA... Has perdido, el número que buscabas es el " + adivinar)
}

//adivinarNumero()

//3.- FUNCION QUE DEVUELVE ARRAY DE N ELEMENTOS

function hacerArray(longitud, valor){
    
    let lista = [0];
    for(let cont = 0; cont < longitud; cont++){
        lista[cont] = valor;
    }
    return lista
}

//console.log(hacerArray(10,1));

//4.- FUNCION QUE DEVUELVE N NUMEROS PRIMOS
 
function listaPrimos(longitud){

    let resultado = [];
    let cont = 0;

    for(let numero = 2; resultado.length <= longitud; numero++){
        let divisor = 2
        for(; numero > divisor; divisor++){
            cont++;
            if(numero % divisor === 0){
                break;
            }
        }
        
        if(divisor === numero){
            resultado.push(numero);
        }
    }
    console.log(cont);
    return resultado;
}

console.log(listaPrimos(1000));


//5. COMPROBAR UN NIF
//00000000X - 8 numeros 1 letra

function comprobarNif(){
    let dni='';
    while(!(/^\d{8}[a-zA-Z]$/.test(dni))){
        dni = prompt("Introduzca un número de DNI: 8 números y una letra");
    }

    //Se separan los números de la letra
    var letraDNI = dni.substring(8, 9).toUpperCase();
    var numDNI = parseInt(dni.substring(0, 8));

    //Se calcula la letra correspondiente al número
    var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
    var letraCorrecta = letras[numDNI % 23];

    if(letraDNI!= letraCorrecta){
    alert("Has introducido una letra incorrecta\nTu letra debería ser: " + letraCorrecta);
    } else {
    alert("Enhorabuena hemos podido validar tu DNI");

    }
}

//console.log(comprobarNif());

//6.-COMPROBAR PALINDROMOS

function palindromos(){
    
    let input = window.prompt("Escribe una frase");
    let esPalindromo = true
    let frase = input.replace(/\s+/g, "").split("");
    let reversa = frase.slice();
    frase.reverse();

    for(let cont = 0; cont <= frase.length; cont++){
        if(frase[cont] != reversa[cont]){
            esPalindromo = false;
            break;
        }
    }

    if(esPalindromo === true){
        alert("La frase SI es un palíndromo");
    } else alert("La frase NO es un palíndromo");
    
}

//palindromos();

function esNIF(nif) {
    if (!/^\d{1,8}[A-Za-z]$/.test(nif))
        return false;
    const letterValue = nif.substr(nif.length - 1).toUpperCase();
    const numberValue = nif.substr(0, nif.length - 1);
    return letterValue === 'TRWAGMYFPDXBNJZSQVHLCKE'.charAt(numberValue % 23);
}
