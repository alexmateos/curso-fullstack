import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { CalculadoraNative } from './componentes/calculadora/calculadora';
import Contador from './componentes/contador/contador';
import Demos from './componentes/demos/demos';

export default function App() {
  return (
    <View>
      <CalculadoraNative/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
