import { StatusBar } from "expo-status-bar";
import React from "react";
import { Text, View, Button } from "react-native";

export class CalculadoraNative extends React.Component {
    
    constructor(props) {
        super(props);
        this.calculadora = {
            valorPantalla: '0',
            primerNum: null,
            segundoNum: null,
            operador: null,
            banderaSegundo: false,
        };
        this.state = { pantalla: this.props.valorPantalla }
        this.meterNumero = this.meterNumero.bind(this);
        this.operar = this.operar.bind(this);
        this.meterDecimal = this.meterDecimal.bind(this);
        this.negativo = this.negativo.bind(this);
        this.borrar = this.borrar.bind(this);
        this.borrarTodo = this.borrarTodo(this);

    }
    actualizar() {
        this.setState({ pantalla: this.calculadora.valorPantalla })
        
    }

    meterNumero(numero) {
        const pantalla = this.calculadora.valorPantalla;
        const banderaSegundo = this.calculadora.banderaSegundo;
        if (banderaSegundo === true) {
            this.calculadora.valorPantalla = numero;
            this.calculadora.banderaSegundo = false;
        } else 
            this.calculadora.valorPantalla = pantalla === '0' ? numero : (pantalla + numero);
        this.actualizar();
    }

    operar(simbolo) {
        const pantalla = this.calculadora.valorPantalla;
        const primerNum = this.calculadora.primerNum;
        const operador = this.calculadora.operador;

        const valorNum = parseFloat(pantalla);

        if (primerNum === null && !isNaN(pantalla)) {
            this.calculadora.primerNum = valorNum;
        } else if (operador) {
            const resultado = this.calcular(primerNum, valorNum, operador);
            this.calculadora.valorPantalla = toString(resultado);
            this.calculadora.primerNum = resultado;
        }

        this.calculadora.banderaSegundo = true;
        this.calculadora.operador = simbolo;

        this.actualizar();
    }

    calcular(primerNum, segNum, operador) {
        switch (operador) {
            case "+":
                return primerNum + segNum;
            case "-":
                return primerNum - segNum;
            case "x":
                return primerNum * segNum;
            case "%":
                return primerNum / segNum;
            case "=":
                return segNum;
            default:
                return;
        }
    }

    borrar() {
        this.calculadora.valorPantalla = "0";
        this.actualizar();
    }

    borrarTodo() {
        this.calculadora.valorPantalla = "0";
        this.calculadora.primerNum = null;
        this.calculadora.segundoNum = null;
        this.calculadora.banderaSegundo = false;
        this.calculadora.operador = null;
        this.actualizar();
    }

    negativo() {
        if (!this.calculadora.valorPantalla.includes("-")) {
            this.calculadora.valorPantalla = "-" + this.calculadora.valorPantalla;
        } else this.calculadora.valorPantalla = this.calculadora.valorPantalla.substring(1);

        this.actualizar();
    }

    meterDecimal(decimal) {
        if (!this.calculadora.valorPantalla.includes(decimal)) {
            this.calculadora.valorPantalla += decimal;
        }
        this.actualizar();
    }

    render() {
        return (
           
            <View>
                <Text>CALCULADORA</Text>
                <StatusBar hidden/>
                <View>
                    <Text >{this.state.pantalla}</Text>

                    <Button title="+" style="operador" onPress={this.operar("+")} />
                    <Button title="-" style="operador" onPress={this.operar("-")} />
                    <Button title="x" style="operador" onPress={this.operar("x")} />
                    <Button title="%" style="operador" onPress={this.operar("%")} />

                    <Button title="7" style="numero" onPress={this.meterNumero("7")} />
                    <Button title="8" style="numero" onPress={this.meterNumero("8")} />
                    <Button title="9" style="numero" onPress={this.meterNumero("9")} />
                    <Button title="CE" style="clearAll" onPress={this.borrarTodo} />

                    <Button title="4" style="numero" onPress={this.meterNumero("4")} />
                    <Button title="5" style="numero" onPress={this.meterNumero("5")} />
                    <Button title="6" style="numero" onPress={this.meterNumero("6")} />
                    <Button title="C" style="clear" onPress={this.borrar} />

                    <Button title="1" style="numero" onPress={this.meterNumero("1")} />
                    <Button title="2" style="numero" onPress={this.meterNumero("2")} />
                    <Button title="3" style="numero" onPress={this.meterNumero("3")} />
                    <Button title="=" style="igual" onPress={this.operar("=")} />

                    <Button title="+/-" style="negativo" onPress={this.negativo} />
                    <Button title="0" style="numero" onPress={this.meterNumero("0")} />
                    <Button title="," style="decimal" onPress={this.meterDecimal(",")} />

                </View>
            </View>
            
        );
    }
}

