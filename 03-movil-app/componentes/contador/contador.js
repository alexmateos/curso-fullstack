import { useState } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default function Contador(props){
    const [contador, setContador] = useState(props.init ? props.init : 0);

    return(
        <View>
            <Text>{contador}</Text>
            <Button value="+1"></Button>
            <Button value="-1"></Button>
        </View>
    )
}